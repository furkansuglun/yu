const express = require('express')
const bodyParser = require('body-parser');
const request = require('request');
var http = require('http');   
var cheerio = require("cheerio"); 
var engine = require('consolidate');

const app = express();

const apiKey = 'bcc46d8aa216979f2505eefa8feda750'

app.use(express.static('public')); //This code allows us to access all of the static files within the ‘public’ folder.
app.use(bodyParser.urlencoded({ extended: true }));
 
app.set('view engine', 'ejs');
 


app.get('/', function(req, res) {
    //res.send('Hello world!')
    res.render('index', {weather: null, error: null})
})

app.post('/', function (req, res) {
    let city = req.body.city;
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&appid=${apiKey}`
  
    request(url, function(error, response, body) {
      if(err){
        res.render('index', {weather: null, error: 'Error, please try again'});
        console.log("error 1")
      } else {
        let weather = JSON.parse(body)
        if(weather.main == undefined){
          res.render('index', {weather: null, error: 'Error, please try again'});
          console.log("error 2")
        } else {
          let weatherText = `It's ${weather.main.temp} degrees in ${weather.name}!`;
          res.render('index', {weather: weatherText, error: null});
          console.log(weatherText)
        }
      }
    });
  })

// TabNine:: https://gitlab.com/furkansuglun/gamenews.git

  app.get('/game', function(req, res) {
    var title = "", subTitle = "";
    
    var newsList = [{news: null}];
    var news = {id: 0, title: "", subTitle: "", author:"", time:"", imagePath:"", description:""};
   

    request("https://www.pcgamer.com/news/", function(error, response, body) {
        
        var $ = cheerio.load(body);

        for (let index = 2; index < 22; index++) {
             var title = $('.result'+index+' > a > article > .content > header > .article-name ').text().trim();
             var subTitle = $('.result'+index+' > a > article > .content > .synopsis').clone().children().remove().end().text().trim();
             var author = $('.result'+index+'  > a > article > .content > header > .byline > .by-author').clone().children().remove().end().text().trim();
             author += $('.result'+index+'  > a > article > .content > header > .byline > .by-author > span').clone().children().remove().end().text().trim();
             var imagePath = $('.result'+index+'  > a > article > .image > figure > div > div > picture > img ').attr('data-srcset').split(' ')[0].trim();

             news = {id: 0, title: "", subTitle: "", author:"", time:"", imagePath:"", description:""};
             news.id = index;
             news.title = title;
             news.subTitle = subTitle;
             news.author = author;
             news.imagePath = imagePath;
 
             newsList.push(news);
        } 


        for (let index = 0; index < newsList.length; index++) {
            console.log("title: " + newsList[index].title + "   subTitle: " + newsList[index].subTitle + "   author: " + newsList[index].author + "  image : " + newsList[index].imagePath);
        }

        res.render('game', {list: newsList})
  
    })

    request("https://www.theverge.com/games", function(error, response, body) {
        
        var $ = cheerio.load(body);

        $('.c-compact-river').children().each(function(index, element) {
           var a = $('.c-entry-box--compact__title > a').text().trim();
           console.log(a);
        })
         
  
    })

    
    
 
  })

  

app.listen('3000', function() {
    console.log('app listening on port 3000')
    console.log('http://localhost:3000')
})


